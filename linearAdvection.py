import numpy.linalg as la
import matplotlib.pyplot as plt
import numpy as np

def FTBS(phi, c, nt):
    '''FTBS is forward in time and backward in space advection scheme,
    phi is initial condition, c is the Courant number,
    nt is number of time steps, 
    using periodic boundary conditions.
    '''
    phiNew = phi.copy()
    nx = len(phi)
    # FTBS for all time steps
    for it in range(nt):
        # loop over all points using modulo arithmetic for periodic boundaries
        for i in range(nx):
            phiNew[i] = phi[i] \
                   -c*(phi[i]-phi[(i-1)%nx])
        # Update phi for next time-step and plot
        phi = phiNew.copy()
    return phi


def CTCS(phi, c, nt):
    '''CTCS is centred in time and centred in space advection scheme,
    phi is initial condition, c is the Courant number,
    nt is number of time steps, 
    using periodic boundary conditions.
    '''
    phiNew = phi.copy()
    phiOld = phi.copy()
    nx = len(phi)
    
    # FTBS for first time step
    phi = FTBS(phiOld, c, 1)
    
    # CTCS for remaining time steps
    for it in range(1,nt):
        # loop over all points using modulo arithmetic for periodic boundaries
        for i in range(nx):
            phiNew[i] = phiOld[i] \
                   -c*(phi[(i+1)%nx]-phi[(i-1)%nx])
        # Update phi for next time-step and plot
        phiOld = phi.copy()
        phi = phiNew.copy()
        
    return phi


def BTCS(phi, c, nt):
    '''BTCS is back in time and center in space for linear advection,
    phi is initial condition, c is Courant number,
    nt is number of time step, using periodic boundary conditions'''
    nx = len(phi)
    # array representing BTCS
    M = np.zeros([nx,nx])
    for i in range(nx):
        M[i,(i-1)%nx] = -c/2
        M[i,i] = 1
        M[i,(i+1)%nx] = c/2
    # BTCS for all time steps
    for it in range(nt):
        # RHS for periodic boundary conditions
        phi = la.solve(M, phi)
    return phi


def BTBS(phi, c, nt):
    '''BTBS is back in time and back space for advection,
    phi is initial condition, c is the Courant number,
    nt is number of time step, using periodic boundary conditions'''
    nx = len(phi)
    # array representing BTBS
    M = np.zeros([nx,nx])
    for i in range(nx):
        M[i,(i-1)%nx] = -c
        M[i,i] = 1+c
    # BTBS for all time steps
    for it in range(nt):
        phi = la.solve(M,phi)
    return phi


def CNCS(phi, c, nt):
    '''CNCS is Crank-Nikolson and center in space for advection,
    phi is initial condition, c is Courant number,
       nt is number of time step, using periodic boundary conditions'''
    nx = len(phi)
    #Create one empty matrixs and set some parameter
    M = np.zeros([nx,nx])
    # create coefficient matrix
    for i in range(nx):
        M[i,(i-1)%nx] = -c/4
        M[i,i] = 1
        M[i,(i+1)%nx] = c/4
    for it in range(nt):
        rhs = phi.copy()
        #create right hand side array and updata by new phi
        for j in range(nx):
            rhs[j] = phi[j%nx] - 0.25*c*(phi[(j+1)%nx]-phi[(j-1)%nx])
        #calculate phi
        phi = la.solve(M, rhs)
    return phi


def Lax_Wendroff(phi, c, nt):
    '''Advects initial condition phi using the Lax-Wendroff method with a
      Courant number of c for nt time steps using periodic boundary conditions.
      phi at the end is returned.'''
    nx = len(phi)
    # new time-step arrays for phi
    phiNew = phi.copy()
    # time steps
    for it in range(nt):
        for j in range(nx):
            # Values at the interfaces
            phiPlus = 0.5*(1+c)*phi[j] + 0.5*(1-c)*phi[(j+1)%nx]
            phiMinus = 0.5*(1+c)*phi[(j-1)%nx] + 0.5*(1-c)*phi[j]
            # Update phi based on interface values
            phiNew[j] = phi[j] - c*(phiPlus - phiMinus)
        # Update ready for next time step
        phi = phiNew.copy()
    return phi


def Semi_Lagrange(phi, c, nt):
    '''Semi-Lagrange advection with cubic Lagrange Interpolation, 
    phi is the initial conditon, c is the Courant number,
    nt is number of time step'''
    nx = len(phi)
    phiNew = phi.copy()
    #loop for all time steps
    for it in range(nt):
        #calculate value at departure point
        for j in range(nx):
            k = int(np.floor(j-c))
            beta = j-k-c
            phiNew[j] = -1/6*beta*(1-beta)*(2-beta)*phi[(k-1)%nx] \
                      + 0.5*(1+beta)*(1-beta)*(2-beta)*phi[k%nx] \
                      + 0.5*(1+beta)*beta*(2-beta)*phi[(k+1)%nx] \
                      - 1/6*(1+beta)*(1-beta)*beta*phi[(k+2)%nx]
        phi = phiNew.copy()
    return phi

def AB(phi, c, nt):
    '''
    AB is third order Adams-Bashforh,phi is the initial conditon, c is the Courant number,nt is number of time step
    '''
    phiNew = phi.copy()
    phi_1 = phi.copy()
    nx = len(phi)
    # lax_Wendroff for first and second time step
    phi_2 = Lax_Wendroff(phi_1,c,1)
    phi_3 = Lax_Wendroff(phi_1,c,2)
    # AB for remaining time steps
    for it in range(2,nt):
        # loop over all points using modulo arithmetic for periodic boundariesb and for spcace using centre difference
        for i in range(nx):
            phiNew[i] = phi_3[i] \
                      - c/2*23/12*(phi_3[(i+1)%nx] - phi_3[(i-1)%nx]) \
                      + c/2*4/3*(phi_2[(i+1)%nx] - phi_2[(i-1)%nx])   \
                      - c/2*5/12*(phi_1[(i+1)%nx] - phi_1[(i-1)%nx])
        # Update phi_1,phi_2,phi_3 for next time-step
        phi_1 = phi_2.copy()
        phi_2 = phi_3.copy()
        phi_3 = phiNew.copy()  
    return phiNew

def LF_4(phi, c, nt):
    '''
    LF_4 is leapfrog time fourth-order space,phi is the initial conditon, c is the Courant number,nt is number of time step.
    '''
    nx = len(phi)
    phiNew = phi.copy()#phiNew is what we want to predict in time step(n+1)
    phi_1 = phi.copy()#phi_1 is bottom layer of time step(n-1)
    # lax_Wendroff for second time step(n)
    phi_2 = Lax_Wendroff(phi_1,c,1)
    # LF_4 for remaining time steps
    for it in range(1,nt):
        # loop over all points using modulo arithmetic for periodic boundariesb
        for i in range(nx):
            phiNew[i] = phi_1[i] \
                        - ((4/3)*c) * (phi_2[(i+1)%nx] - phi_2[(i-1)%nx])\
                        + ((1/6)*c) * (phi_2[(i+2)%nx] - phi_2[(i-2)%nx])
        # Update phi_1,phi_2 for next time-step
        phi_1 = phi_2.copy()
        phi_2 = phiNew.copy()
    return phiNew
#According to my test,when c<0.73,this scheme is stable.

def CNTS3(phi, c, nt):
    '''CNTS3 is Crank-Nikolson in time and Taylor series approximation in third order
       for advection, phi is initial condition, c is Courant number,
       nt is number of time step, using periodic boundary conditions'''
    nx = len(phi)
    #Create one empty matrixs and set some parameter
    M = np.zeros([nx,nx])
    # create coefficient matrix
    for i in range(nx):
        M[i,(i-1)%nx] = -c/2
        M[i,i] = 1+c/4
        M[i,(i+1)%nx] = c/6
        M[i,(i-2)%nx] = c/12
    for it in range(nt):
        rhs = phi.copy()
        #create right hand side array and updata by new phi
        for j in range(nx):
            rhs[j] = (1-c/4) * phi[j%nx] - (c/6) * phi[(j+1)%nx] + (c/2) * phi[(j-1)%nx]\
                     - (c/12) * phi[(j-2)%nx]
        #calculate phi
        phi = la.solve(M, rhs)
    return phi

def CNSA4(phi, c, nt):
    '''CNSA4 is Crank-Nikolson in time and Fourth order space approximation 
       in space for advection, phi is initial condition, c is Courant number,
       nt is number of time step, using periodic boundary conditions'''
    nx = len(phi)
    #Create one empty matrixs and set some parameter
    M = np.zeros([nx,nx])
    # create coefficient matrix
    for i in range(nx):
        M[i,(i-1)%nx] = -c/3
        M[i,i] = 1
        M[i,(i+1)%nx] = c/3
        M[i,(i-2)%nx] = c/24
        M[i,(i+2)%nx] = -c/24
    for it in range(nt):
        rhs = phi.copy()
        #create right hand side array and updata by new phi
        for j in range(nx):
            rhs[j] = phi[j%nx] - (c/3) * phi[(j+1)%nx] + (c/3) * phi[(j-1)%nx]\
                     - (c/24) * phi[(j-2)%nx] + (c/24) * phi[(j+2)%nx]
        #calculate phi
        phi = la.solve(M, rhs)
    return phi

def cosineBell(x):
    '''Return a cosine bell for initial conditions of advection as a function
    of space, x'''
    return np.where(x%1 < 0.5, 0.5*(1-np.cos(4*np.pi*x)), 0)

def cosineBellSqr(x):
    '''Return a cosine bell for initial conditions of advection as a function
    of space, x'''
    return np.where(x%1 < 0.5, (0.5*(1-np.cos(4*np.pi*x)))**2, 0)


def topHat(x):
    '''Return a top hat for initial conditions of advection as a function
    of space, x'''
    return np.where(x%1 < 0.5, 1, 0)


def L2ErrorNorm(phi, phiExact):
    """Calculates the L2 error norm (RMS error) of phi in comparison to
    phiExact"""
    return np.sqrt(sum((phi - phiExact)**2)/sum(phiExact**2))


def main(cfl,nx):
    nx = nx
    dx = 1/nx
    cfl = cfl
    u = 1.
    dt = cfl*dx/u
    total_T = 1
    nt = int((total_T+1e-12)/dt)
    initialConditions = cosineBellSqr
    '''
    # Check that dt*nt = total time
    if abs(nt*dt - total_T) > 1e-10:
        print('dt = ', dt, ' nt = ', nt, ' dt*nt = ', dt*nt,
              ' Total requested time = ', total_T)
        raise Exception("Time step does not exactly divide total time")
    '''
    x = np.arange(0, 1, dx)
    phi = initialConditions(x)
    
    #Calculate and plot
    FTBS_1 = FTBS(phi.copy(), cfl, nt)
    CTCS_1 = CTCS(phi.copy(), cfl, nt)
    BTBS_1 = BTBS(phi.copy(), cfl, nt)
    BTCS_1 = BTCS(phi.copy(), cfl, nt)
    CNCS_1 = CNCS(phi.copy(), cfl, nt)
    Semi_Lagrange_1 = Semi_Lagrange(phi.copy(), cfl, nt)
    Lax_Wendroff_1 = Lax_Wendroff(phi.copy(), cfl, nt)
    Exact_1 = initialConditions(x - nt*cfl/nx)
    #TLSISL_1=TLSISL(phi.copy(), cfl, 1,0.9)#The scheme also very goood,even after long time its error still very small
    #and generally bigger a and less error
    AB_1=AB(phi.copy(),cfl,nt)
    LF4_1=LF_4(phi.copy(),cfl,nt)#LF4 is stable when c<0.73
    CNTS3_1=CNTS3(phi.copy(),cfl,nt)
    CNSA4_1=CNSA4(phi.copy(),cfl,nt)
    
    plt.subplots(figsize=(16,9))
    plt.style.use('seaborn')
    #plt.plot(x,phi)
    plt.tick_params(axis='both',labelsize=30)
    plt.plot(x, Exact_1, label = 'Exact',c='black')
    plt.plot(x, FTBS_1, label = 'FTBS',c='peru')
    plt.plot(x, CTCS_1, label = 'CTCS')
#    plt.plot(x, BTBS_1, label = 'BTBS')
    #plt.plot(x, CNCS_1, label = 'CNCS')
    plt.plot(x, Semi_Lagrange_1, label = 'Semi-Lagrangian',c='red',marker='D')
    plt.plot(x, BTCS_1, label = 'BTCS',c='orchid')
    #plt.plot(x, Lax_Wendroff_1, label='Lax Wendroff')
#    #plt.plot(x, TLSISL_1, label='TLSISL_1')
#    plt.plot(x, AB_1, label='AB_1')
    #plt.plot(x, LF4_1, '*g-',label='LF4_1')
    plt.plot(x, CNTS3_1,label='CNTS3',c='blue')
    plt.plot(x, CNSA4_1,label='CNSA4',c='green',marker='D')
    plt.title('Numerical and exact solution C=2.5 $\Delta$x=80',fontsize=40)
    plt.xlabel('x',fontsize=30)
    plt.ylabel(r'$\phi$',fontsize=30)
    plt.legend(fontsize=30)
    plt.ylim([-0.2,1.2])
    #plt.savefig('C:/Users/Wangjiang Gong/Desktop/毕业论文/plot and paper/Result12.tiff',bbox_inches='tight')
    #plt.show()

    #Calculate errors
    BTBS1_error = L2ErrorNorm(BTBS_1, Exact_1)
    FTBS1_error = L2ErrorNorm(FTBS_1, Exact_1)
    BTCS1_error = L2ErrorNorm(BTCS_1, Exact_1)
    CNCS1_error = L2ErrorNorm(CNCS_1, Exact_1)
    SL_error = L2ErrorNorm(Semi_Lagrange_1, Exact_1)
    Lax_Wendroff1_error = L2ErrorNorm(Lax_Wendroff_1, Exact_1)
    #TLSISL_1error =L2ErrorNorm(TLSISL_1, Exact_1)
    LF4_error=L2ErrorNorm(LF4_1, Exact_1)
    AB1_error = L2ErrorNorm(AB_1, Exact_1)
    CNSA4_error=L2ErrorNorm(CNSA4_1, Exact_1)
    CNTS3_error=L2ErrorNorm(CNTS3_1, Exact_1)
    CTCS_error=L2ErrorNorm(CTCS_1, Exact_1)
    error_list=[BTBS1_error,FTBS1_error,BTCS1_error,CNCS1_error,SL_error
                ,Lax_Wendroff1_error,LF4_error,CNSA4_error,CNTS3_error]
    return error_list
'''
main(0.1,40)
main(0.8,40)
main(1.6,40)
main(2.5,40)

main(0.1,20)
main(0.8,20)
main(1.6,20)
main(2.5,20)

main(0.1,80)
main(0.8,80)
main(1.6,80)
main(2.5,80)
'''
 
    
nxs = np.array([20,40,80])
dxs = 1/nxs
cfls = np.array([0.1,0.8, 1.6, 2.5])
#calculate different numerical method's error at different cfl and nx is 10,20,30,40,50,60.
error_matrix1 = np.zeros([len(cfls),len(nxs),9])
for inx in range(len(nxs)):
    for ic in range(len(cfls)):
        dx = dxs[inx]
        nx = nxs[inx]
        cfl = cfls[ic]
        error_matrix1[ic,inx,:] = main(cfl,nx)
plt.subplot(221)        
#plt.subplots(figsize=(16,9))
'''
#error at cfl=0.1
#plt.loglog(dxs,error_matrix1[0,:,3],label = 'CNCS')
plt.loglog(dxs,error_matrix1[0,:,4],label = 'SL')
#plt.loglog(dxs,error_matrix1[0,:,5],label = 'Lax_Wendroff')
#plt.loglog(dxs,error_matrix1[0,:,6],label = 'LF4')
plt.loglog(dxs,error_matrix1[0,:,7],label = 'CNSA4')
plt.loglog(dxs,error_matrix1[0,:,8],label = 'CNTS3')
plt.tick_params(direction='inout',width=2,length=6,axis='both')
plt.title('a',fontsize=10)
plt.ylabel('log(Error)',fontsize=8)
plt.legend()

##error at cfl=0.2
plt.subplot(222)
#plt.subplots(figsize=(16,9))

#plt.loglog(dxs,error_matrix1[1,:,3],label = 'CNCS')
plt.loglog(dxs,error_matrix1[1,:,4],label = 'SL')
#plt.loglog(dxs,error_matrix1[1,:,5],label = 'Lax_Wendroff')
#plt.loglog(dxs,error_matrix1[1,:,6],label = 'LF4')
plt.loglog(dxs,error_matrix1[1,:,7],label = 'CNSA4')
plt.loglog(dxs,error_matrix1[1,:,8],label = 'CNTS3')
plt.title('b',fontsize=10)
plt.ylabel('log(Error)',fontsize=8)
plt.tick_params(direction='inout',width=2,length=6,axis='both')
#plt.tick_params(axis='both',labelsize=6)
plt.legend()

plt.subplot(223)
#plt.subplots(figsize=(16,9))

##error at cfl=0.5
#plt.loglog(dxs,error_matrix1[2,:,3],label = 'CNCS')
plt.loglog(dxs,error_matrix1[2,:,4],label = 'SL')
#plt.loglog(dxs,error_matrix1[2,:,5],label = 'Lax_Wendroff')
#plt.loglog(dxs,error_matrix1[2,:,6],label = 'LF4')
plt.loglog(dxs,error_matrix1[2,:,7],label = 'CNSA4')
plt.loglog(dxs,error_matrix1[2,:,8],label = 'CNTS3')
plt.title('c',fontsize=10)
plt.tick_params(direction='inout',width=2,length=6,axis='both')
plt.ylabel('log(Error)',fontsize=8)
plt.xlabel('log(dx)',fontsize=10)
plt.legend()

plt.subplot(224)
#plt.subplots(figsize=(16,9))
##error at cfl=1 expect LF4
#plt.loglog(dxs,error_matrix1[3,:,3],label = 'CNCS')
plt.loglog(dxs,error_matrix1[3,:,4],label = 'SL')
#plt.loglog(dxs,error_matrix1[3,:,5],label = 'Lax_Wendroff')
#plt.loglog(dxs,error_matrix1[3,:,6],label = 'LF4')
plt.loglog(dxs,error_matrix1[3,:,7],label = 'CNSA4')
plt.loglog(dxs,error_matrix1[3,:,8],label = 'CNTS3')
plt.tick_params(direction='inout',width=2,length=6,axis='both')
plt.title('d',fontsize=10)
plt.ylabel('log(Error)',fontsize=8)
plt.xlabel('log(dx)',fontsize=10)
plt.legend()
#plt.savefig('C:/Users/Wangjiang Gong/Desktop/毕业论文/plot and paper/error.tiff',bbox_inches='tight')
plt.show()
'''



###below part is amplification factor and phase speed plot process
#parameter set and create empty matrix
velocity=1
cfl_array=np.linspace(0,2.5,101)#y coordinate
kdx_array=np.linspace(0,np.pi,101)#x coordinate
matrix_amplification1 = np.zeros([len(cfl_array),len(kdx_array)],dtype=complex)#matrix for CNTS3
matrix_amplification2 = np.zeros([len(cfl_array),len(kdx_array)],dtype=complex)#matrix for CNSA4
matrix_kdt=np.zeros([len(cfl_array),len(kdx_array)])#matrix for k*dt
#calculate amplification factor for CNSA4  and CNTS3 and phase speed
for i in range(len(cfl_array)):
    for n in range(len(kdx_array)):
            c=cfl_array[i]
            kdx=kdx_array[n]
            matrix_amplification1[i,n]=(1-c/4-c/6*np.exp(1j*kdx)\
                                      + c/2*np.exp(-1j*kdx)\
                                      - c/12*np.exp(-2*1j*kdx))\
                                      / (c/12*np.exp(-2*1j*kdx)  \
                                      - c/2*np.exp(-1j*kdx)
                                      +1+c/4+c/6*np.exp(1j*kdx))
            matrix_amplification2[i,n]=(1 - c/3*np.exp(1j*kdx)\
                                          + c/3*np.exp(-1j*kdx)\
                                          - c/24*np.exp(-2*1j*kdx)\
                                          + c/24*np.exp(1j*2*kdx))\
                                      / (1 - c/3*np.exp(-1j*kdx)\
                                         + c/3*np.exp(1j*kdx)\
                                         + c/24*np.exp(-2*1j*kdx)\
                                         - c/24*np.exp(1j*2*kdx))
            matrix_kdt[i,n]=c*kdx/velocity 

plt.subplots(figsize=(16,9))
pic1=plt.contourf(kdx_array,cfl_array,np.abs(matrix_amplification1),cmap=plt.cm.RdBu_r,levels=np.linspace(0,1,10))
plt.colorbar(pic1,orientation='horizontal',format='%.1f',cmap=plt.cm.RdBu_r)   
plt.title('Magnitude of amplification factor for CNTS3',fontsize=30)
plt.xlabel('k$\Delta$x',fontsize=20)
plt.ylabel('CFL',fontsize=20)
#plt.savefig('C:/Users/Wangjiang Gong/Desktop/毕业论文/plot and paper/AF for CNTS3.tiff',bbox_inches='tight')

plt.subplots(figsize=(16,9))
pic2=plt.contourf(kdx_array,cfl_array,np.abs(matrix_amplification2),cmap=plt.cm.RdBu_r)
plt.colorbar(pic2,orientation='horizontal',format='%.1f')     
plt.title('Magnitude of amplification factor for CNSA4',fontsize=30)
plt.xlabel('k$\Delta$x',fontsize=20)
plt.ylabel('CFL',fontsize=20)
#plt.savefig('C:/Users/Wangjiang Gong/Desktop/毕业论文/plot and paper/AF for CNSA4.tiff',bbox_inches='tight')


#phase speed plot, this is new, I have change line 480,line501
phase_speed=np.arctan2(-matrix_amplification1.imag,matrix_amplification1.real)/(matrix_kdt*velocity)

plt.subplots(figsize=(16,9))
pic3=plt.contourf(kdx_array,cfl_array,phase_speed,cmap=plt.cm.RdBu_r,levels=np.linspace(0,1,10))
plt.colorbar(pic3,orientation='horizontal',format='%.1f')  
plt.title('Phase speed of CNTS3 for CFL in range from 0 to 2.5',fontsize=30)
plt.xlabel('k$\Delta$x',fontsize=20)
plt.ylabel('CFL',fontsize=20)
plt.savefig('C:/Users/Wangjiang Gong/Desktop/毕业论文/plot and paper/phase speed 2D-CNTS3.tiff',bbox_inches='tight')

plt.subplots(figsize=(16,9))
plt.plot(kdx_array,phase_speed[4,:],label='cfl=0.1')
plt.plot(kdx_array,phase_speed[32],label='cfl=0.8')
plt.plot(kdx_array,phase_speed[64],label='cfl=1.6')
plt.plot(kdx_array,phase_speed[100],label='cfl=2.5')
plt.ylabel('u/u$_n$',fontsize=20)
plt.xlabel('k$\Delta$x',fontsize=20)
plt.title('Numerical phase speed of CNSA3',fontsize=20)
plt.legend()
#plt.savefig('C:/Users/Wangjiang Gong/Desktop/毕业论文/plot and paper/phase_speed-CNTS3.tiff',bbox_inches='tight')

phase_speed1=np.arctan2(-matrix_amplification2.imag,matrix_amplification2.real)/(matrix_kdt*velocity)

plt.subplots(figsize=(16,9))
pic4=plt.contourf(kdx_array,cfl_array,phase_speed1,cmap=plt.cm.RdBu_r,levels=np.linspace(0,1,10))
plt.colorbar(pic4,orientation='horizontal',format='%.1f')  
plt.title('Phase speed of CNSA4 for CFL in range from 0 to 2.5',fontsize=30)
plt.xlabel('k$\Delta$x',fontsize=20)
plt.ylabel('CFL',fontsize=20)
#plt.savefig('C:/Users/Wangjiang Gong/Desktop/毕业论文/plot and paper/phase_speed-CNSA4-2D.tiff',bbox_inches='tight')

plt.subplots(figsize=(16,9))
plt.plot(kdx_array,phase_speed1[4],label='cfl=0.1')
plt.plot(kdx_array,phase_speed1[32],label='cfl=0.8')
plt.plot(kdx_array,phase_speed1[64],label='cfl=1.6')
plt.plot(kdx_array,phase_speed1[100],label='cfl=2.5')
plt.ylabel('u/u$_n$',fontsize=20)
plt.xlabel('k$\Delta$x',fontsize=20)
plt.title('Numerical phase speed of CNSA4',fontsize=20)
plt.legend()
#plt.savefig('C:/Users/Wangjiang Gong/Desktop/毕业论文/plot and paper/phase_speed-CNSA4.tiff',bbox_inches='tight')




