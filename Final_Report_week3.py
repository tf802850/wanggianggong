import numpy.linalg as la
import matplotlib.pyplot as plt
import numpy as np
import math

def FTBS(phiOld, c, nt):
#HW All functions need doc-strings
    nx = len(phiOld)
    phi = phiOld.copy()
#HW This comment is not consistent with the code
    # FTCS for all time steps
    for it in range(nt):
        # loop over all internal points
        for i in range(1,nx):
            phi[i] = phiOld[i] \
                   -c*(phiOld[i]-phiOld[i-1])
        #periodic boundary condition
        phi[0] = phi[-1]
        # Update phi for next time-step and plot
        phiOld=phi.copy()
    return phi

def BTCS(phiold, c, nt):
    nx = len(phiold)
    # array representing BTCS
    M = np.zeros([nx,nx])
    phi = phiold.copy()
    #periodic boundary condition
    M[0,0] = 1.
    M[0,-1] = -c/2.
    M[0,1]=c/2
    M[-1,0]=c/2
    M[-1,-1]=1
    M[-1,-2]=-c/2
    for i in range(1,nx-1):
        M[i,i-1] = -c/2
        M[i,i] = 1
        M[i,i+1] = c/2
    # BTCS for all time steps
    for it in range(nt):
        # RHS for periodic boundary conditions
        phi = la.solve(M,phiold)
        #plt.plot(x, phi ,'black',lw =0.2,label= 'FTBS')
        phiold=phi.copy()
    return phi

def BTBS(phiold, c, nt):
    nx = len(phiold)
    # array representing BTBS
    M = np.zeros([nx,nx])
    phi = phiold.copy()
    # periodic boundary condition
    M[0,0] = 1+c
    M[0,-1] = -c
    for i in range(1,nx):
        M[i,i-1] = -c
        M[i,i] = 1+c
    # BTBS for all time steps
    for it in range(nt):
        # RHS for periodic boundary conditions
        phi = la.solve(M,phiold)
        phiold=phi.copy()
    return phi

def CNCS(phiold, c, nt):
    '''
    In order to implement CNCS scheme.We need to create two matrix,Here is matrix M 
    and matrix N,and multiply both side by inverse of N,then we can get M*inv(N)*phi[n+1]=
    phi[n].Finally, we get the iterative formula.
    '''
    #Create two empty matrixs and set some parameter
    nx = len(phiold)
    M = np.zeros([nx,nx])
    N=np.zeros([nx,nx])
    phi = phiold.copy()
    # periodic boundary condition and create two matrixs M and N
#HW Work out how to implement CNCS using only one matrix
    M[0,0] = 1.
    M[0,-1]=-c/4
    M[0,1]=c/4
    M[-1,-1]=1
    M[-1,-2]=-c/4
    M[-1,0]=c/4
    N[0,0]=1
    N[0,-1]=c/4
    N[0,1]=-c/4
    N[-1,-1]=1
    N[-1,-2]=c/4
    N[-1,0]=-c/4
    for i in range(1,nx-1):
        M[i,i-1] = -c/4
        M[i,i] = 1
        M[i,i+1] = c/4
    for j in range(1,nx-1):
        N[j,j-1] = c/4
        N[j,j] = 1
        N[j,j+1] = -c/4
#HW You should not calculate a matrix inverse when solving a PDE. This is too
#HW computationally expensive. Use la.solve only
    N_inv=la.inv(N) #Inverse of Matrix N 
    for it in range(nt):
        # RHS for periodic boundary conditions
        phi = la.solve(np.dot(M,N_inv),phiold)
        phiold=phi.copy()
    return phi

#def Lax_Wendroff(phiold,c,nt):
##HW This infor should be in a comment rather than a doc-string. Doc-strings
##HW should describe what the function does and what all the arguments and 
##HW output are;
#    '''the key point for implement Lax_Wendroff is calcualte the phi value
#       at space step i+1/2 at time n+1/2,so I use FTBS to calcualte phi value at space step
#       i+1/2,set dt and dx is half of original one
#     '''   
##HW This is not how Lax_Wendroff should be implemented. You should not use the
##HW initial conditions at every time step and you shoudl not use FTBS. I have
##HW put my implementation of Lax_Wendroff below
#    nx = len(phiold)
#    phi = phiold.copy()
#    for it in range(nt):
#        lax_semi=FTBS(initial_condition(space_lax)[1],cfl/2,2*it+1)#calculate phi value at i+1/2 and n+1/2
#        # loop over all internal points
#        for i in range(1,nx-1):
#            phi[i] = phiold[i] -c*(lax_semi[2*i]-lax_semi[2*i-1])
#        phiold=phi.copy()
#        phi[0] = phi[-1]
#    return phi

def Lax_Wendroff(phi,c,nt):
    '''Advects initial condition phi using the Lax-Wendroff method with a
      Courant number of c for nt time steps using periodic boundary conditions.
      phi at the end is returned.'''
    nx = len(phi)

    # new time-step arrays for phi
    phiNew = phi.copy()
    
    # time steps
    for it in range(nt):
        for j in range(nx):
            # Values at the interfaces
            phiPlus = 0.5*(1+c)*phi[j] + 0.5*(1-c)*phi[(j+1)%nx]
            phiMinus = 0.5*(1+c)*phi[(j-1)%nx] + 0.5*(1-c)*phi[j]
            # Update phi based on interface values
            phiNew[j] = phi[j] - c*(phiPlus - phiMinus)
        # Update ready for next time step
        phi = phiNew.copy()

    return phi


def initial_condition(x=[]):
    '''there are three initial condition a is sinx,b is 0.5*(1-4*cosx)in period 
    from 0 to 0.5 and 0 in period from 0.5 to 1,c is 1 form 0 to 0.5 and 0 
    from 0.5 to 1'''
    a=np.sin(x)
#HW do not initialise b to be an empty list. Never initialise an empty list
#HW when you know in advance how long an array will need to be. Do not use
#HW append. Instead use b[i] = np.zeros_like(x)
    b=[]
#HW Do not use i for a specific value of x. i shoudl be an array index. Use xi
#HW instead. ie
#HW for xi in x:
#HW or
#HW for i in range(len(x)):
#HW    b[i] = 0.5*(1-np.cos(4*math.pi*x[i]))
#HW
    for i in x:
        if i<0.5:
            b.append(0.5*(1-np.cos(4*math.pi*i)))
        else:
            b.append(0)
    c=[]
    for j in x:
        if j<0.5:
            c.append(1)
        else:
            c.append(0)
    return a,b,c

#HW Combine your analytic function with the initial condition. They are closely
#HW related. See code below:
#HW Exact_1 = initial_condition((space - u*nt*dt)%1.)[1]
def analytic1(x,t,u):
    Analytic=[]
    for i in t:
        Analytic=[]
        for j in x:
            if j<0.5:
                Analytic.append(0.5*(1-np.cos(4*math.pi*(j-u*i))))
            else:
                Analytic.append(0)
        Analytic[0]=Analytic[-1]
        plt.plot(x,Analytic,'r',lw=0.2)
    return Analytic

def analytic2(x,t,u):
    Analytic=[]
    for i in t:
        Analytic=[]
        for j in x:
            if j<0.5:
                Analytic.append(1)
            else:
                Analytic.append(0)
        Analytic[0]=Analytic[-1]
        plt.plot(x,Analytic,'green',lw=0.2)    
    return Analytic

def L2ErrorNorm(phi, phiExact):
    """Calculates the L2 error norm (RMS error) of phi in comparison to
    phiExact, ignoring the boundaries"""
    #remove one of the end points
    phi = phi[1:-1]
    phiExact = phiExact[1:-1]
    # calculate the error and the error norms
    phiError = phi - phiExact
    L2 = np.sqrt(sum(phiError**2)/sum(phiExact**2))
    return L2

nx=41
dx = 1/(nx-1)
u = 1 
cfl = 0.1
dt=cfl*dx/u
#HW Make this print statement more informative or remove it
print(dt)
totol_T=1.0
nt = int((totol_T+1e-12)/dt) # number of time steps
#HW Make this print statement more informative or remove it
print(nt)
space=np.arange(0,1+dx,dx)
#HW Make this print statement more informative or remove it
print(space)
time=np.arange(0,totol_T+dt,dt)
#HW Make this print statement more informative or remove it
print(time)
phiold=initial_condition(space)
nRevolutions = dt*nt*u
print('Running for ', dt*nt, ' time. ', nRevolutions, ' revolutions')
#HW I do not understand what this array is for
space_lax=np.arange(0,1+dt/2,dt/2)
print(space_lax)

#Calculate and plot
FTBS_1=FTBS(phiold[1], cfl, nt)
BTBS_1=BTBS(phiold[1],cfl,nt)
BTCS_1=BTCS(phiold[1],cfl,nt)
CNCS_1=CNCS(phiold[1],cfl,nt)
Lax_Wendroff_1=Lax_Wendroff(phiold[1],cfl,nt)
Exact_1 = initial_condition((space - u*nt*dt)%1.)[1]
plt.plot(space, FTBS_1, label='FTBS')
plt.plot(space, BTBS_1, label='BTBS')
plt.plot(space, Exact_1, label='exact')
plt.plot(space, BTCS_1, label='BTCS')
plt.plot(space,CNCS_1,label='CNCS')
plt.plot(space,Lax_Wendroff_1,label='Lax_Wendroff')
plt.legend()
plt.show()

#Calculate error
BTBS1_error = L2ErrorNorm(np.array(BTBS_1),np.array(Exact_1))
FTBS1_error = L2ErrorNorm(np.array(FTBS_1),np.array(Exact_1))
BTCS1_error = L2ErrorNorm(np.array(BTCS_1),np.array(Exact_1))
CNCS1_error = L2ErrorNorm(np.array(CNCS_1),np.array(Exact_1))
Lax_Wendroff1_error = L2ErrorNorm(np.array(Lax_Wendroff_1),np.array(Exact_1))

print('BTBS1_error = ', BTBS1_error)
print('FTBS1_error = ', FTBS1_error)
print('BTCS1_error = ', BTCS1_error)
print('CNCS1_error = ', CNCS1_error)
print('Lax_Wendroff1_error = ',Lax_Wendroff1_error)
