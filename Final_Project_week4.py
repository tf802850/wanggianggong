import numpy.linalg as la
import matplotlib.pyplot as plt
import numpy as np
import math
def FTBS(phiold, c, nt,nx):
    '''FTBS is forward in time and backward in space,phiold is initial condition,c is crount number,
       nt is number of time step,nx is number of space,using periodic boundary conditions.
    '''
    phi = phiold.copy()
    # FTBS for all time steps
    for it in range(nt):
        # loop over all internal points
        for i in range(1,nx):
            phi[i] = phiold[i] \
                   -c*(phiold[i]-phiold[i-1])
        #periodic boundary condition
        phi[0] = phi[-1]
        # Update phi for next time-step and plot
        phiold=phi.copy()
    return phi

def BTCS(phiold, c, nt,nx):
    '''BTCS is back in time and center in space,phiold is initial condition,c is crount number,
       nt is number of time step,nx is number of space, using periodic boundary conditions'''
    # array representing BTCS
    M = np.zeros([nx,nx])
    phi = phiold.copy()
    #periodic boundary condition
    M[0,0] = 1.
    M[0,-1] = -c/2.
    M[0,1]=c/2
    M[-1,0]=c/2
    M[-1,-1]=1
    M[-1,-2]=-c/2
    for i in range(1,nx-1):
        M[i,i-1] = -c/2
        M[i,i] = 1
        M[i,i+1] = c/2
    # BTCS for all time steps
    for it in range(nt):
        # RHS for periodic boundary conditions
        phi = la.solve(M,phiold)
        phiold=phi.copy()
    return phi

def BTBS(phiold, c, nt,nx):
    '''BTBS is back in time and back space,phiold is initial condition,c is crount number,
       nt is number of time step,nx is number of space step, using periodic boundary conditions'''
    # array representing BTBS
    M = np.zeros([nx,nx])
    phi = phiold.copy()
    # periodic boundary condition
    M[0,0] = 1+c
    M[0,-1] = -c
    for i in range(1,nx):
        M[i,i-1] = -c
        M[i,i] = 1+c
    # BTBS for all time steps
    for it in range(nt):
        phi = la.solve(M,phiold)
        phiold=phi.copy()
    return phi

def CNCS(phiold, c, nt,nx):
    '''CNCS is crank-nikson and center in space,phiold is initial condition,c is crount number,
       nt is number of time step,nx is number of space step, using periodic boundary conditions'''
    #Create one empty matrixs and set some parameter
    M = np.zeros([nx,nx])
    phi = phiold.copy()
    # create coefficient matrix
    for i in range(nx):
        M[i,(i-1)%nx] = -c/4
        M[i,i] = 1
        M[i,(i+1)%nx] = c/4
    for it in range(nt):
        rhs=phi
        #create right hand side array and updata by new phi
        for j in range(nx):
            rhs[j]=phi[j%nx]-(c/4)*(phi[(j+1)%nx]-phi[(j-1)%nx])
        #calculate phi
        phi = la.solve(M,rhs)
    return phi

def Lax_Wendroff(phi,c,nt,nx):
    '''Advects initial condition phi using the Lax-Wendroff method with a
      Courant number of c for nt time steps using periodic boundary conditions.
      phi at the end is returned.'''
    # new time-step arrays for phi
    phiNew = phi.copy()
    # time steps
    for it in range(nt):
        for j in range(nx):
            # Values at the interfaces
            phiPlus = 0.5*(1+c)*phi[j] + 0.5*(1-c)*phi[(j+1)%nx]
            phiMinus = 0.5*(1+c)*phi[(j-1)%nx] + 0.5*(1-c)*phi[j]
            # Update phi based on interface values
            phiNew[j] = phi[j] - c*(phiPlus - phiMinus)
        # Update ready for next time step
        phi = phiNew.copy()
    return phi

'''Confusion of semi Lagrangian method: according to your handout, I can calculate value at the Department point, 
but this value is the value on the previous time axis. How can I deduce the value on the next time axis 
from the initial value?'''
def Semi_Lagrange_1(phiold,c,nt,nx):
    '''this is implement Semi-Lagrange with cubic Interpolation ,phiold is initial conditon,c is courant number,
    nt is number of time step,nx is number of space step'''
    #create empty array for k parameter and empty array for beta parameter 
    k=np.zeros(nx)
    beta=np.zeros(nx)
    f=phiold.copy()
    #calculate k and beta
    for j in range(nx): 
        k[j]=np.floor(j-c)
        beta[j]=j-k[j]-c
    k=k.astype(int)
    fx=np.zeros(len(k))
    #loop for all time steps
    for it in range(nt):
        #calculate value at departure point
        for i in range(len(k)-1): 
            fx[i]=(-1/6)*beta[i]*(1-beta[i])*(2-beta[i])*f[k[i]-1]+(1/2)*(1+beta[i])*(1-beta[i])*(2-beta[i])*f[k[i]]\
            +(1/2)*(1+beta[i])*beta[i]*(2-beta[i])*f[k[i]+1]-(1/6)*(1+beta[i])*(1-beta[i])*beta[i]*f[k[i]+2]
        f=fx
    return fx

'''The former method(Semi_Lagrange_1) may be wrong. I thought of another possibility as below, because the Lagrange method focuses on the 
   movement of a point, so the change should be the spatial coordinates'''
def Semi_Lagrange_2(phiold,c,nt,nx,velocity,t,space_):
    '''this is implement Semi-Lagrange with cubic Interpolation ,phiold is initial conditon,c is courant number,
    nt is number of time step,nx is number of space step,velocity is u,t is total time,space_ is space coordinate system'''
    #create empty array for k parameter and empty array for beta parameter and f is initial condition
    k=np.zeros(nx)
    beta=np.zeros(nx)
    f=phiold.copy()
    #calculate k and beta
    for j in range(nx):  
        k[j]=np.floor(j-c)
        beta[j]=j-k[j]-c
    k=k.astype(int)
    fx=np.zeros(len(k))
    #loop for all time steps
    for it in range(nt):
        #calculate value at departure point
        for i in range(len(k)-1): 
            fx[i]=(-1/6)*beta[i]*(1-beta[i])*(2-beta[i])*f[k[i]-1]+(1/2)*(1+beta[i])*(1-beta[i])*(2-beta[i])*f[k[i]]\
            +(1/2)*(1+beta[i])*beta[i]*(2-beta[i])*f[k[i]+1]-(1/6)*(1+beta[i])*(1-beta[i])*beta[i]*f[k[i]+2]
    space_ax=space_+velocity*t
    return fx,space_ax

def initial_condition(x,nx):
    '''there are three initial condition a is sinx,b is 0.5*(1-4*cosx)in period 
    from 0 to 0.5 and 0 in period from 0.5 to 1,c is 1 form 0 to 0.5 and 0 
    from 0.5 to 1,x is space array,nx is number of space'''
    a=np.zeros(nx)
    for k in range(nx):
        a[k]=np.sin(x[k])
    b=np.zeros(nx)
    for i in range(nx):
        if i*dx<0.5:
            b[i]=0.5*(1-np.cos(4*math.pi*x[i]))
        else:
            b[i]=0
    c=np.zeros(nx)
    for j in range(nx):
        if nx*dx<0.5:
            c[j]=1
        else:
            c[j]=0
    return a,b,c

def L2ErrorNorm(phi, phiExact):
    """Calculates the L2 error norm (RMS error) of phi in comparison to
    phiExact, ignoring the boundaries"""
    #remove one of the end points
    phi = phi[1:-1]
    phiExact = phiExact[1:-1]
    # calculate the error and the error norms
    phiError = phi - phiExact
    L2 = np.sqrt(sum(phiError**2)/sum(phiExact**2))
    return L2

'''I have change those parameters name to avoid global value'''
number_x=41
dx = 1/(number_x-1)
u = 1 
cfl = 0.1
dt=cfl*dx/u
totol_T=1
number_time = int((totol_T+1e-12)/dt) 

space=np.arange(0,1+dx,dx)
time=np.arange(0,totol_T+dt,dt)
phiold_array=initial_condition(space,number_x)
#Calculate and plot
FTBS_1=FTBS(phiold_array[1], cfl, number_time,number_x)
BTBS_1=BTBS(phiold_array[1],cfl,number_time,number_x)
BTCS_1=BTCS(phiold_array[1],cfl,number_time,number_x)
CNCS_1=CNCS(phiold_array[1],cfl,number_time,number_x)
Semi_Lagrange_1=Semi_Lagrange_1(phiold_array[1],cfl,number_time,number_x)
Semi_Lagrange_2=Semi_Lagrange_2(phiold_array[1],cfl,number_time,number_x,u,totol_T,space)
Lax_Wendroff_1=Lax_Wendroff(phiold_array[1],cfl,number_time,number_x)
Exact_1 = initial_condition((space - u*number_time*dt)%1.,number_x)[1]
plt.plot(space, FTBS_1, label='FTBS')
plt.plot(space, BTBS_1, label='BTBS')
plt.plot(space, Exact_1, label='exact')
plt.plot(space, BTCS_1, label='BTCS')
plt.plot(space,CNCS_1,label='CNCS')
plt.plot(space,Semi_Lagrange_1,label='Semi_1')
plt.plot(space,Lax_Wendroff_1,label='lax')
plt.plot(Semi_Lagrange_2[1],Semi_Lagrange_2[0],label='Semi_2')
plt.legend()
plt.show()

#Calculate error
BTBS1_error = L2ErrorNorm(np.array(BTBS_1),np.array(Exact_1))
FTBS1_error = L2ErrorNorm(np.array(FTBS_1),np.array(Exact_1))
BTCS1_error = L2ErrorNorm(np.array(BTCS_1),np.array(Exact_1))
CNCS1_error = L2ErrorNorm(np.array(CNCS_1),np.array(Exact_1))
Lax_Wendroff1_error = L2ErrorNorm(np.array(Lax_Wendroff_1),np.array(Exact_1))

print('BTBS1_error = ', BTBS1_error)
print('FTBS1_error = ', FTBS1_error)
print('BTCS1_error = ', BTCS1_error)
print('CNCS1_error = ', CNCS1_error)
print('Lax_Wendroff1_error = ',Lax_Wendroff1_error)

